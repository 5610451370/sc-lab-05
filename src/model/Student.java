package model;

import java.util.ArrayList;

public class Student {	
	String id;
	String name;
	Student stu;
	
	public Student(String name, String id){ 
		this.id =id;
		this.name = name;
	}
	
	public String getName(){ 
		return name;
	}
	
	public String getId(){ 
		return id;
	}
	
	public void setName(Student stu){
		this.name = name;
	}

	public void setId(Student stu){
		this.id =id;
	}
}
