package model;

public class Book {
	String namebook;
	String publish;
	Book book;
	
	public Book(String namebook,String publish){
		this.publish = publish;
		this.namebook = namebook;
	}
	
	public String getBookname(){
		return namebook;
	}
	
	public String getpublish(){
		return publish;
	}
	
	public void setBookname(Book book){
		 this.namebook = namebook;
	}
	
	public void setPublish(Book book){
		 this.publish = publish;
	}
	
}
