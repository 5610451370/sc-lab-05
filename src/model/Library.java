package model;

import java.util.ArrayList;

public class Library {
	Book book;
	Magazine mag;
	String bookname;
	Student stu;
	Teacher teacher;
	ReferenceBook refbook;
	Staffku staff;
	ArrayList arraybook = new ArrayList();	
	ArrayList arraymag = new ArrayList();
	ArrayList arraystuborrow = new ArrayList();
	ArrayList arrayteacherborrow = new ArrayList();
	ArrayList arraystaffborrow = new ArrayList();
	
	public void addBook(Book book){
		book.setBookname(book);
		book.setPublish(book);
		this.arraybook.add(book.getBookname()+" Publish "+book.getpublish());
	}
	
	public void addMagazine(Magazine mag){
		mag.setMagname(mag);
		mag.setPublish(mag);
		this.arraymag.add(mag.getMagname()+" Publish "+mag.getpublish());
	}
	
	
	public void addReferenceBook(ReferenceBook refbook){   // Add Reference Book
		refbook.setRefBookname(refbook);
		this.arraybook.add(refbook.getrefbook());
	}
	
	public ArrayList getShowbooks(){  
		return arraybook;
	}
	public ArrayList getShowmag(){  
		return arraymag;
	}
	
	public void borrowStudent(Student stu,String bookname){   // Borrow Book for Student
		stu.setName(stu);
		stu.setId(stu);
		arraystuborrow.add(stu.getName()+" : "+stu.getId()+" , Borrow : "+bookname);
		if (bookname == "PC Referrence Book" || bookname == "PC Referrence Magazine"){
			arraystuborrow.remove(0);
		}
		else{
			arraybook.remove(0);
		}
	}
	
	public ArrayList getBorrowStudent(){
		return arraystuborrow;
	}
	
	public void returnStubook(Student stu,String bookname){   // Return Book for Student
		stu.setName(stu);
		stu.setId(stu);	
		arraybook.add(bookname);
		arraystuborrow.remove(0);
		arraystuborrow.add(stu.getName()+" : "+stu.getId()+" , Return : "+bookname);
	}
	public void returnStaffmag(Staffku staff,String bookname){   // Return Book for Student
		staff.setName(staff);	
		arraymag.add(bookname);
		arraystaffborrow.remove(0);
		arraystaffborrow.add(staff.getName()+" , Return : "+bookname);
	}
	
	public ArrayList getReturnStudent(){ 
		return arraystuborrow;
	}
	public ArrayList getReturnStaff(){ 
		return arraystaffborrow;
	}
	
	public int getCountbook(){  // Count Books
		return arraybook.size();
	}
	public int getCountMag(){  // Count Magazine
		return arraymag.size();
	}

	public void borrowTeacher(Teacher teacher,String bookname){   // Borrow Magazine for Teacher
		teacher.setName(teacher);
		teacher.setId(teacher);
		arrayteacherborrow.add(teacher.getName()+" : "+teacher.getId()+" , Borrow : "+bookname);
		if (bookname == "PC Referrence Book" || bookname == "PC Referrence Magazine"){
			arrayteacherborrow.remove(0);
		}
		else{
			arraymag.remove(0);
		}
	}
	
	public ArrayList getBorrowTeacher(){
		return arrayteacherborrow;
	}

	public void borrowStaff(Staffku staff,String bookname){   // Borrow Magazine for staff
		staff.setName(staff);
		arraystaffborrow.add(staff.getName()+" , Borrow : "+bookname);
		if (bookname == "PC Referrence Book" || bookname == "PC Referrence Magazine"){
			arraystaffborrow.remove(0);
		}
		else{
			arraymag.remove(0);
		}
	}
	public ArrayList getBorrowStaff(){
		return arraystaffborrow;
	}
}
