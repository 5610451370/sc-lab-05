package controll;

import model.Book;
import model.Library;
import model.Magazine;
import model.ReferenceBook;
import model.Staffku;
import model.Student;
import model.Teacher;

public class Main {
	public static void main(String[] args) {
		Library library = new Library(); 
		
		Student student = new Student("Surathep Srasamlee","5610451370");
		Teacher teacher = new Teacher("Usa Sammapan","D14");
		Staffku staff = new Staffku("Tongdee Boonmak");

		Book book01 = new Book("Big Java","2010");
		Book book02 = new Book("Big Python","2013");
		Book book03 = new Book("Doraemon","1999");
		Magazine mag01 = new Magazine("Home and Garden","2015");
		Magazine mag02 = new Magazine("Marvel","2014");
		Magazine mag03 = new Magazine("Women Style","2012");
		
		ReferenceBook refbook = new ReferenceBook("PC Referrence Book"); // Don't borrow 
		ReferenceBook refmag = new ReferenceBook("PC Referrence Magazine"); // Don't borrow 
		
		library.addBook(book01);
		library.addBook(book02);
		library.addBook(book03);
		library.addMagazine(mag01);
		library.addMagazine(mag02);
		library.addMagazine(mag03);
		library.addReferenceBook(refbook);
		
		System.out.println("List of book to borrow : "+library.getShowbooks());
		System.out.println("Quantity of books = "+library.getCountbook());
		
		System.out.println("--------------------------------------------------------------------");
		library.borrowStudent(student, "Big Java");
		System.out.println(library.getBorrowStudent());
		System.out.println("Quantity of books = "+library.getCountbook());
		library.returnStubook(student, "Big Java");
		System.out.println(library.getReturnStudent());
		System.out.println("Quantity of books = "+library.getCountbook());
		library.borrowStudent(student, "PC Referrence Book");
		System.out.println(library.getBorrowStudent());
		System.out.println("Quantity of books = "+library.getCountbook());
		
		System.out.println("--------------------------------------------------------------------");
		System.out.println("List of Magazine to borrow : "+library.getShowmag());
		System.out.println("Quantity of Magazines = "+library.getCountMag());
		System.out.println("--------------------------------------------------------------------");
		library.borrowTeacher(teacher, "Home and Garden");
		System.out.println(library.getBorrowTeacher());
		System.out.println("Quantity of Magazines = "+library.getCountMag());
		library.borrowTeacher(teacher, "PC Referrence Magazine");
		System.out.println(library.getBorrowTeacher());
		System.out.println("Quantity of Magazines = "+library.getCountMag());
		library.borrowStaff(staff, "Marvel");
		System.out.println(library.getBorrowStaff());
		System.out.println("Quantity of Magazines = "+library.getCountMag());
		library.returnStaffmag(staff, "Marvel");
		System.out.println(library.getReturnStaff());
		System.out.println("Quantity of Magazines = "+library.getCountMag());
		System.out.println("--------------------------------------------------------------------");
	}

}
